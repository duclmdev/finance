import {Tabs, Tab} from "./Tabs";
import {LoanInterestRate} from "./LoanInterestRate";
import {SavingInterestRate} from "./SavingInterestRate";
import {IncomeTax} from "./IncomeTax";

function App() {
  return (
    <Tabs>
      <Tab component={<LoanInterestRate />}></Tab>
      <Tab component={<SavingInterestRate />}></Tab>
      <Tab component={<IncomeTax />}></Tab>
    </Tabs>
  );
}

export default App;
