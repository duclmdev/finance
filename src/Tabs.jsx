import {createSignal, children, For} from "solid-js";

export function Tabs(props) {
  const c = children(() => props.children);
  return (
    <>
      <ul id="tabs" class="tab tab-block">
        <li class="tab-item active">
          <a href="#tab-loan-interest-rate">Lãi suất vay</a>
        </li>
        <li class="tab-item">
          <a href="#tab-saving-interest-rate">Lãi suất tiết kiệm</a>
        </li>
        <li class="tab-item">
          <a href="#tab-income-tax">Thuế thu nhập cá nhân</a>
        </li>
      </ul>
      <div id="tab-loan-interest-rate">tab-loan-interest-rate</div>
      <div id="tab-saving-interest-rate" class="d-hide">
        tab-saving-interest-rate
      </div>
      <div id="tab-income-tax" class="d-hide">
        tab-income-tax
      </div>
    </>
  );
}

export function Tab() {}
